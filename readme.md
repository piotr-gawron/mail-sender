This code allows sending separate emails to set of email addresses using personalized data. Copy of every email is sent also to `FROM` address. 

# Usage
Clone repository:

```bash
git clone https://gitlab.lcsb.uni.lu/piotr.gawron/mail-sender.git
cd mail-sender
```

Create configuration from template:
```bash
cp config.py.template config.py 
```
Change `config.py`:
* LOGIN - login to the SMTP server (it might be the same as sender email, but it's not necessary)
* PASSWORD - password to the SMTP server
* HOST - host of SMTP server
* PORT - port of SMTP server
* SSL - use ssl
* TLS - use tls
* SUBJECT - mail subject
* FROM - sender email
* TXT_TEMPLATE - file with email text template
* HTML_TEMPLATE - file with html text template
* RECIPIENTS - csv file with recipients and personalized fields

Send emails:
```bash
python3 send.py 
```

# Mail templates

Text mail template (`TXT_TEMPLATE`) is obligatory. Some clients don't support html mail or disable html for security reason. In such situation text content is provided to the user.

`RECIPIENTS` file should contain at least one column with email: `EMAIL`.

Mail templates can contain placeholders that will be replaced with data from `RECIPIENTS` file. The placeholders should be surrounded by `%%` characters. For example, if in `RECIPIENTS` file we have column named `USER_NAME` then every occurrence of `%%USER_NAME%%` will be replaced with the proper value. 
